
#include "UR_pbx_pstn_Operator_details_info.h"

UR_pbx_pstn_Operator_details_info::UR_pbx_pstn_Operator_details_info()//default cons
	{
		operator_name = "";
        codec = "";
        vtcode = "";
		
	}
	
	
	UR_pbx_pstn_Operator_details_info::~UR_pbx_pstn_Operator_details_info()//destructor
	{
		
	}
	
	
	UR_pbx_pstn_Operator_details_info::UR_pbx_pstn_Operator_details_info(UR_pbx_pstn_Operator_details_info &obj2)//copy constructor
	{
		operator_name = obj2.operator_name;
        codec = obj2.codec;
        vtcode = obj2.vtcode;  
	}
	
     void UR_pbx_pstn_Operator_details_info:: operator=(UR_pbx_pstn_Operator_details_info &obj3)//assignment operator
     {
        operator_name = obj3.operator_name;
        codec = obj3.codec;
        vtcode = obj3.vtcode;
	}
                
        

	
