#include<iostream>
#include<string>
using namespace std;

#include "sip_pbx_new_check_conference.h"

sip_pbx_new_check_conference::sip_pbx_new_check_conference()//default cons
	{
		conf_usr_type = 0;
		Extension_Number = "";
		errcode = 0;
		errmsg = "";
		extn_audio_file = "";
	}
	
	
	sip_pbx_new_check_conference::~sip_pbx_new_check_conference()//destructor
	{
		
	}
	
	
	sip_pbx_new_check_conference::sip_pbx_new_check_conference(sip_pbx_new_check_conference &obj2)//copy constructor
	{
	    conf_usr_type = obj2.conf_usr_type;
		Extension_Number = obj2.Extension_Number;
		errcode = obj2.errcode;
		errmsg = obj2.errmsg;
		extn_audio_file = obj2.extn_audio_file;
	}
	
	void  sip_pbx_new_check_conference::operator=(sip_pbx_new_check_conference &obj3)//assignment operator
       {
        conf_usr_type = obj3.conf_usr_type;
		Extension_Number = obj3.Extension_Number;
		errcode = obj3.errcode;
		errmsg = obj3.errmsg;
		extn_audio_file = obj3.extn_audio_file;
	}
                
       


	
