#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libmemcached/memcached.h>
#include <iostream>
//#include <fstream> 
#include <sstream>
class my_class
{
private:
    double m_x;
    double m_y;
    int num;
    char* name;
    char ch;

public:
    my_class(double a=0.0, double b=0.0, int c=0,char* d = "no_name",char e = 'D' )
      : m_x(a), m_y(b), num(c),name(d),ch(e) {
    }
    std::string serialize(/*std::fstream& fs*/)
    {
        std::string to_write;
        std::ostringstream ss;

        ss << "{ " << "\"m_x\": " << m_x << ", "
                   << "\"m_y\": " << m_y << ", "
		   << "\"num\": " << num << ", "
      		   << "\"name\": " << name << ", "
                   << "\"ch\": " << ch
           << " }";

        
        to_write = ss.str();
        return to_write;
    }

    friend std::ostream& operator<< (std::ostream &out, const my_class &my_obj);
};

std::ostream& operator<< (std::ostream &out, const my_class &my_obj)
{
    
    out << "my_class(" << my_obj.m_x << ", " << my_obj.m_y << ", " << my_obj.num << ", " << my_obj.name << ", " << my_obj.ch <<')'; 
    return out; 
}
    
int main()
{
    memcached_server_st *servers = NULL;//used to create a list of memcached servers
    memcached_st *memc;
    memcached_return rc;
    char *key= "keystring";
    char *retrieved_value;
    uint32_t flags;


    /*takes a string, the type that is used for the
    command line applications, and parse it to an array of
    memcached_server_st.*/
    memcached_server_st  *memcached_servers_parse (const char *server_strings);
    memc= memcached_create(NULL);//for creating structure
    servers= memcached_server_list_append(servers, "localhost", 11211, &rc);//adds server to end of the array(memached_server_st)
    rc= memcached_server_push(memc, servers);//pushes an array of memcached_server_st into the memcached_st structure.
    if (rc == MEMCACHED_SUCCESS)//The request was successfully executed
    fprintf(stderr,"Added server successfully\n");
  else
    fprintf(stderr,"Couldn't add server: %s\n",memcached_strerror(memc, rc));

    //std::fstream fs("Points.txt", std::ios::out);
    my_class my_obj(2.0, 3.0, 4,"Dayana",'D');

   // std::cout << my_obj << '\n';
    std::cout << my_obj.serialize() << std::endl;
    std::string str = my_obj.serialize();
    std::cout << str << std::endl;
    char* c = const_cast<char*>(str.c_str());
    std::cout << c << std::endl;
    size_t value_length = strlen(c);
    //fs.close();
    rc= memcached_set(memc, key, strlen(key), c, strlen(c), (time_t)0, (uint32_t)0);
    if (rc == MEMCACHED_SUCCESS)
    fprintf(stderr,"Key stored successfully\n");
    else
    fprintf(stderr,"Couldn't store key: %s\n",memcached_strerror(memc, rc));

     retrieved_value = memcached_get(memc,key,strlen(key), &value_length, &flags, &rc);
     if (rc == MEMCACHED_SUCCESS) {
    fprintf(stderr, "Data retrieved from key successfully\n");
    printf("The key '%s' returned Data '%s'.\n",key, retrieved_value);
    free(retrieved_value);
  }
  else
    fprintf(stderr, "Couldn't retrieve Data: %s\n", memcached_strerror(memc, rc));

  //  std::fstream fs1("Points.txt", std::ios::in);
   // my_class p = 
//    std::cout << my_obj.deserialize(fs1) << std::endl;
//std::cout << p << std::endl;
    return 0;
}
    
