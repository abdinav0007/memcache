#include <stdio.h>
#include <sql.h>
#include <sqlext.h>
#include <string>
#include <iostream>
using namespace std;
/*
 *  * see Retrieving ODBC Diagnostics
 *   * for a definition of extract_error().
 *    */
/*
 *  * see Retrieving ODBC Diagnostics
 *   * for a definition of extract_error().
 *    */
static void extract_error(char *fn,SQLHANDLE handle,SQLSMALLINT type);
int MySQLSuccess(SQLRETURN rc);
void printTable(const struct DataBinding* tableResult);

int main()
{
  SQLHENV env;
  SQLHDBC dbc;
  SQLHSTMT stmt;
  SQLRETURN ret; /* ODBC API return status */
  SQLCHAR connectstr[1024+1],connectstring_out[1024+1];
  SQLSMALLINT outstrlen;
  SQLSMALLINT columns; /* number of columns in result-set */
  SQLCHAR buf[ 5 ][ 64 ];
  int row = 0;
  SQLINTEGER indicator[ 5 ];
  int i;
 UCHAR errmsg[50]= "";
  
  /*
  string db_SERVER_ipaddress="10.22.2.86";
  string UID="smepbx";
  string password="smeswitch"; 
  string DATABASE="unifiedring";
  
  
  
  char db_SERVER_ipaddress[]="10.22.2.86";
  char UID[]="smepbx";
  char password[]="smeswitch"; 
  char DATABASE[]="unifiedring";

*/
  char* db_SERVER_ipaddress=(char*)"10.22.2.86";
  char* UID=(char*)"smepbx";
  char* password=(char*)"smeswitch"; 
  char* DATABASE=(char*)"unifiedring";

//sprintf(connectstr,"DRIVER=FreeTDS;SERVER=%s;UID=%s;PWD=%s;DATABASE=%s;TDS_version=7.0;port=1433;",db_SERVER_ipaddress.c_str(), UID.c_str(), password.c_str(), DATABASE.c_str());

sprintf((char *)connectstr,(char *)"DRIVER=FreeTDS;SERVER=%s;UID=%s;PWD=%s;DATABASE=%s;TDS_version=7.0;port=1433;",db_SERVER_ipaddress, UID, password,DATABASE);


  /* Allocate an environment handle */
  SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
  
  /* We want ODBC 3 support */
  SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0);
  
  /* Allocate a connection handle */
  SQLAllocHandle(SQL_HANDLE_DBC, env, &dbc);
  
  
  /* Connect to the DSN mydsn */
  ret = SQLDriverConnect(dbc, NULL, connectstr, SQL_NTS,connectstring_out, sizeof(connectstring_out), &outstrlen, SQL_DRIVER_COMPLETE);
//SQLDriverConnect(dbc, NULL, "DSN=fred;", SQL_NTS,outstr, sizeof(outstr), &outstrlen,SQL_DRIVER_COMPLETE);
						 
  if (SQL_SUCCEEDED(ret))
  {
    printf("Connected\n");
    printf("Returned connection string was:\n\t%s\n", connectstring_out);
	
    if (ret == SQL_SUCCESS_WITH_INFO)
		
	{
      printf("Driver reported the following diagnostics\n");
      extract_error((char*)"SQLDriverConnect", dbc, SQL_HANDLE_DBC);
    }
    //SQLDisconnect(dbc);               /* disconnect from driver */
  } 
  
  else 
  {
    fprintf(stderr, "Failed to connect\n");
    extract_error((char*)"SQLDriverConnect", dbc, SQL_HANDLE_DBC);
  }
  
  
  /* Allocate a statement handle */
   ret=SQLAllocHandle(SQL_HANDLE_STMT, dbc, &stmt);
   
   
   
   if(ret!= SQL_SUCCESS) {
    SQLError(env, dbc, SQL_NULL_HSTMT, NULL, NULL, errmsg, sizeof(errmsg), NULL);
    printf("SQLAllocHandle failed with sql errmsg: %s\n", errmsg);
    return false;	
  }
   
   /* Retrieve a list of tables */

   //SQLTables(&stmt, NULL, 0, NULL, 0, "'call_q'", 0, (SQLCHAR *)"", NULL);
   
   //ret = SQLTables(stmt, (SQLCHAR*)"msdb",SQL_NTS, (SQLCHAR*)"dbo",SQL_NTS, (SQLCHAR*)SQL_ALL_TABLE_TYPES,SQL_NTS, (SQLCHAR*)"'call_q'", SQL_NTS );
  
   SQLTables(stmt,(SQLCHAR*)SQL_ALL_CATALOGS, SQL_NTS,(SQLCHAR*)"",0,(SQLCHAR*)"",0,(SQLCHAR*)NULL,0);
//    SQLTables(stmt, NULL, 0, NULL, 0, NULL, 0, "TABLE", SQL_NTS);


 SQLExecDirect(stmt,(SQLCHAR*)"select top 10 * from call_q;", SQL_NTS);
//SQLExecDirect(stmt,"show columns from [table name]", SQL_NTS);
	

 // SQLExecDirect(stmt,(SQLCHAR*) "exec sip_pbx_new_get_extension_info_new '211', '213','7204.UR.mundio.com'",SQL_NTS);

				 
/*							 
        for ( ret = SQLFetch(stmt) ;
              MySQLSuccess(ret) ;
              ret = SQLFetch(stmt) ) {
            printTable( tableResult );
      }
  */
     
 /*  
   SQLRETURN SQLTables(  
     SQLHSTMT       StatementHandle,  
     SQLCHAR *      CatalogName,  
     SQLSMALLINT    NameLength1,  
     SQLCHAR *      SchemaName,  
     SQLSMALLINT    NameLength2,  
     SQLCHAR *      TableName,  
     SQLSMALLINT    NameLength3,  
     SQLCHAR *      TableType,  
     SQLSMALLINT    NameLength4);  
   
   
   */
   
   /* How many columns are there */

   SQLNumResultCols(stmt, &columns);
  
   /* local variables */
   
   
  // for (i = 0; i < columns; i++) {
  //    SQLBindCol( &stmt, i + 1, SQL_C_CHAR, buf[ i ], sizeof( buf[ i ] ), (SQLLEN*)&indicator[ i ] );
  //        
  //        //QLBindCol(hstmt, 1, SQL_C_CHAR, SalesPerson, sizeof(SalesPerson),&SalesPersonLenOrInd);
  // }
   
      /* Fetch the data */
   while (SQL_SUCCEEDED(SQLFetch(&stmt))) 
   {
      /* display the results that will now be in the bound area's */
      for ( i = 0; i < columns; i ++ )
      {
         SQLLEN indi;
         char buffer[512];
         SQLLEN retValue = SQLGetData(stmt, i ,SQL_C_CHAR, buffer, sizeof(buffer), &indi);
         if (indi == SQL_NULL_DATA) 
	 {
            printf("  Column %u : NULL\n", i);
         }
         else 
		 {
            printf("  Column %u : %s\n", i, buffer);
         }
      }
   }
  
  
  
  /* free up allocated handles */
  SQLFreeHandle(SQL_HANDLE_DBC, dbc);
  SQLFreeHandle(SQL_HANDLE_ENV, env);
}

void extract_error(
    char *fn,
    SQLHANDLE handle,
    SQLSMALLINT type)
{
    SQLINTEGER   i = 0;
    SQLINTEGER   native;
    SQLCHAR      state[ 7 ];
    SQLCHAR      text[256];
    SQLSMALLINT  len;
    SQLRETURN    ret;

    fprintf(stderr,
            "\n"
            "The driver reported the following diagnostics whilst running "
            "%s\n\n",
            fn);

    do
    {
        ret = SQLGetDiagRec(type, handle, ++i, state, &native, text,sizeof(text), &len );
		
        if (SQL_SUCCEEDED(ret))
            printf("%s:%ld:%ld:%s\n", state, i, native, text);
    }
    while( ret == SQL_SUCCESS );
}


#if 0
int MySQLSuccess(SQLRETURN rc) {
   return (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO);
}


void printTable(const struct DataBinding* tableResult) {

    // index 0 - catalog e.q. msdb
    // index 1 - schema  e.g dbo
    // index 2 - table name e.g. TestTBL1
    // index 3 - type - e.g. TABLE

    if (tableResult[2].StrLen_or_Ind != SQL_NULL_DATA)
        printf("Table (%s) = %s\n", (char *)tableResult[1].TargetValuePtr,
                                    (char *)tableResult[2].TargetValuePtr);
}


void printTable(const struct DataBinding* tableResult) {

    // index 0 - catalog e.q. msdb
    // index 1 - schema  e.g dbo
    // index 2 - table name e.g. TestTBL1
    // index 3 - type - e.g. TABLE

    if (tableResult[2].StrLen_or_Ind != SQL_NULL_DATA)
        printf("Table (%s) = %s\n", (char *)tableResult[1].TargetValuePtr,
                                    (char *)tableResult[2].TargetValuePtr);
}

#endif
