#include<iostream>
#include<string>
using namespace std;

#include "UR_PBX_get_company_hours_setting_v2.h"

UR_PBX_get_company_hours_setting_v2::UR_PBX_get_company_hours_setting_v2()//default cons
	{
		working_business_type = 0;
                company_hour = 0;
		hour_format = 0;
		comp_hours_ivr_type = 0;
		comp_hours_ivr_file = "";
		comp_after_hours_ivr_type = 0;
		comp_after_hours_ivr_file = ""; 
		Greeting_Play_Type = 0;
		Comp_After_Greeting_Play_Type = 0;
		comp_hour_extension = "";
		comp_hours_after_extension = "";
		no_action = 0;
		no_action_after = 0;
		auto_routing = 0;
		switcboardid = 0;
		opr_extension = "";
		sb_welcome_prompt = "";
		
	}
	
	
	UR_PBX_get_company_hours_setting_v2::~UR_PBX_get_company_hours_setting_v2()//destructor
	{
		
	}
	
	
	UR_PBX_get_company_hours_setting_v2::UR_PBX_get_company_hours_setting_v2(UR_PBX_get_company_hours_setting_v2 &obj2)//copy constructor
	{
	    working_business_type = obj2.working_business_type;
        company_hour = obj2.company_hour;
		hour_format = obj2.hour_format;
		comp_hours_ivr_type = obj2.comp_hours_ivr_type;
		comp_hours_ivr_file = obj2.comp_hours_ivr_file;
		comp_after_hours_ivr_type = obj2.comp_after_hours_ivr_type;
		comp_after_hours_ivr_file = obj2.comp_after_hours_ivr_file; 
		Greeting_Play_Type = obj2.Greeting_Play_Type;
		Comp_After_Greeting_Play_Type = obj2.Comp_After_Greeting_Play_Type;
		comp_hour_extension = obj2.comp_hour_extension;
		comp_hours_after_extension = obj2.comp_hours_after_extension;
		no_action = obj2.no_action;
		no_action_after = obj2.no_action_after;
		auto_routing = obj2.auto_routing;
		switcboardid = obj2.switcboardid;
		opr_extension = obj2.opr_extension;
		sb_welcome_prompt = obj2.sb_welcome_prompt;
	}
	
 void UR_PBX_get_company_hours_setting_v2:: operator=(UR_PBX_get_company_hours_setting_v2 &obj3)//assignment operator
     {
        working_business_type = obj3.working_business_type;
        company_hour = obj3.company_hour;
		hour_format = obj3.hour_format;
		comp_hours_ivr_type = obj3.comp_hours_ivr_type;
		comp_hours_ivr_file = obj3.comp_hours_ivr_file;
		comp_after_hours_ivr_type = obj3.comp_after_hours_ivr_type;
		comp_after_hours_ivr_file = obj3.comp_after_hours_ivr_file; 
		Greeting_Play_Type = obj3.Greeting_Play_Type;
		Comp_After_Greeting_Play_Type = obj3.Comp_After_Greeting_Play_Type;
		comp_hour_extension = obj3.comp_hour_extension;
		comp_hours_after_extension = obj3.comp_hours_after_extension;
		no_action = obj3.no_action;
		no_action_after = obj3.no_action_after;
		auto_routing = obj3.auto_routing;
		switcboardid = obj3.switcboardid;
		opr_extension = obj3.opr_extension;
		sb_welcome_prompt = obj3.sb_welcome_prompt;
	}
