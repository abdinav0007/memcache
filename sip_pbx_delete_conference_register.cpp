#include"sip_pbx_delete_conference_register.h"
sip_pbx_delete_conference_register::sip_pbx_delete_conference_register()
{
	conf_id="";
	call_id="";
	conf_status="";
	conf_mg_ip_address="";
	pbx_ip_address="";
	connection_time="";
	participant_type=0;
	create_date="";
	last_update="";
	domain_name="";
	conf_mg_uid="";
		
}
sip_pbx_delete_conference_register::~sip_pbx_delete_conference_register()
{
}

sip_pbx_delete_conference_register::sip_pbx_delete_conference_register(const sip_pbx_delete_conference_register &sip_pbx_delete1)
{
	conf_id=sip_pbx_delete1.conf_id;
	call_id=sip_pbx_delete1.call_id;
	conf_status=sip_pbx_delete1.conf_status;
	conf_mg_ip_address=sip_pbx_delete1.conf_mg_ip_address;
	pbx_ip_address=sip_pbx_delete1.pbx_ip_address;
	connection_time=sip_pbx_delete1.connection_time;
	participant_type=sip_pbx_delete1.participant_type;
	create_date=sip_pbx_delete1.create_date;
	last_update=sip_pbx_delete1.last_update;
	domain_name=sip_pbx_delete1.domain_name;
	conf_mg_uid=sip_pbx_delete1.conf_mg_uid;
}
void sip_pbx_delete_conference_register::operator=(const sip_pbx_delete_conference_register &sip_pbx_delete2)
{
	conf_id=sip_pbx_delete2.conf_id;
	call_id=sip_pbx_delete2.call_id;
	conf_status=sip_pbx_delete2.conf_status;
	conf_mg_ip_address=sip_pbx_delete2.conf_mg_ip_address;
	pbx_ip_address=sip_pbx_delete2.pbx_ip_address;
	connection_time=sip_pbx_delete2.connection_time;
	participant_type=sip_pbx_delete2.participant_type;
	create_date=sip_pbx_delete2.create_date;
	last_update=sip_pbx_delete2.last_update;
	domain_name=sip_pbx_delete2.domain_name;
	conf_mg_uid=sip_pbx_delete2.conf_mg_uid;
} 
