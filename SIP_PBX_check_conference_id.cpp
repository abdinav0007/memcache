#include"SIP_PBX_check_conference_id.h"

SIP_PBX_check_conference_id::SIP_PBX_check_conference_id()//default constructor
{
	 errcode=0;
         errmsg="";
	 hostpin="";
	 participant_pin="";
	 screening_enabled=0;
	 recording_enabled=0;
	 conf_expired=0;
}
SIP_PBX_check_conference_id::~SIP_PBX_check_conference_id()
{}
SIP_PBX_check_conference_id::SIP_PBX_check_conference_id(const SIP_PBX_check_conference_id &PBX_conference1)//copy constructor
{
	 errcode=PBX_conference1.errcode;
     errmsg=PBX_conference1.errmsg;
	 hostpin=PBX_conference1.hostpin;
	 participant_pin=PBX_conference1.participant_pin;
	 screening_enabled=PBX_conference1.screening_enabled;
	 recording_enabled=PBX_conference1.recording_enabled;
	 conf_expired=PBX_conference1.conf_expired;
	
}
void SIP_PBX_check_conference_id::operator=(const SIP_PBX_check_conference_id &PBX_conference2)//assignment constructor
{
	errcode=PBX_conference2.errcode;
     errmsg=PBX_conference2.errmsg;
	 hostpin=PBX_conference2.hostpin;
	 participant_pin=PBX_conference2.participant_pin;
	 screening_enabled=PBX_conference2.screening_enabled;
	 recording_enabled=PBX_conference2.recording_enabled;
	 conf_expired=PBX_conference2.conf_expired;
}
