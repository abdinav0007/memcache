#include<iostream>
#include<string>
using namespace std;

#include "sip_pbx_get_conference_register_info.h"

sip_pbx_get_conference_register_info::sip_pbx_get_conference_register_info()//default cons
	{
		call_id = "";
		conf_status = "";
		conf_mg_ip_address = "";
		pbx_ip_address = ""; 
		connection_time = "";
		participant_type = 0;
		conf_mg_uid = "";
		rec_count = 0;	
	}
	
	
	sip_pbx_get_conference_register_info::~sip_pbx_get_conference_register_info()//destructor
	{
	
	}
	
	
	sip_pbx_get_conference_register_info::sip_pbx_get_conference_register_info(sip_pbx_get_conference_register_info &obj2)//copy constructor
	{
	    call_id = obj2.call_id;
		conf_status = obj2.conf_status;
		conf_mg_ip_address = obj2.conf_mg_ip_address;
		pbx_ip_address = obj2.pbx_ip_address; 
		connection_time = obj2.connection_time;
		participant_type = obj2.participant_type;
		conf_mg_uid = obj2.conf_mg_uid;
		rec_count = obj2.rec_count;	
	}
	
     void sip_pbx_get_conference_register_info:: operator=(sip_pbx_get_conference_register_info &obj3)//assignment operator
     {
                call_id = obj3.call_id;
		conf_status = obj3.conf_status;
		conf_mg_ip_address = obj3.conf_mg_ip_address;
		pbx_ip_address = obj3.pbx_ip_address; 
		connection_time = obj3.connection_time;
		participant_type = obj3.participant_type;
		conf_mg_uid = obj3.conf_mg_uid;
		rec_count = obj3.rec_count;	 
	}
                
        

	
