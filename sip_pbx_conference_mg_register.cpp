#include<iostream>
#include<string>
using namespace std;

#include "sip_pbx_conference_mg_register.h"

        sip_pbx_conference_mg_register::sip_pbx_conference_mg_register()//default cons
	{
		conf_mg_ip_address = "";
	}
	
	
	sip_pbx_conference_mg_register::~sip_pbx_conference_mg_register()//destructor
	{
		
	}
	
	
	sip_pbx_conference_mg_register::sip_pbx_conference_mg_register(sip_pbx_conference_mg_register &obj2)//copy constructor
	{
	   conf_mg_ip_address = obj2.conf_mg_ip_address;
	}
	
	void  sip_pbx_conference_mg_register:: operator=(sip_pbx_conference_mg_register &obj3)//assignment operator
        {
       conf_mg_ip_address = obj3.conf_mg_ip_address;
	}
                
        

	
