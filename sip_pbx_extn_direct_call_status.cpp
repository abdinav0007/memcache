#include"sip_pbx_extn_direct_call_status.h"

sip_pbx_extn_direct_call_status::sip_pbx_extn_direct_call_status()
{
	 is_enable=0;
	 is_pin_req=0;
	 pin="";
}
sip_pbx_extn_direct_call_status::~sip_pbx_extn_direct_call_status()
{}

sip_pbx_extn_direct_call_status::sip_pbx_extn_direct_call_status(const sip_pbx_extn_direct_call_status &sip_pbx_extn_direct1)
{
	 is_enable=sip_pbx_extn_direct1.is_enable;
	 is_pin_req=sip_pbx_extn_direct1.is_pin_req;
	 pin=sip_pbx_extn_direct1.pin;
}

void sip_pbx_extn_direct_call_status::operator=(const sip_pbx_extn_direct_call_status &sip_pbx_extn_direct2 )
{
	is_enable=sip_pbx_extn_direct2.is_enable;
	 is_pin_req=sip_pbx_extn_direct2.is_pin_req;
	 pin=sip_pbx_extn_direct2.pin;
}