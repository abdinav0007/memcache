
class sip_pbx_get_conference_register_info
{
	public:
	    string call_id;
		string conf_status;
		string conf_mg_ip_address;
		string pbx_ip_address;
		string connection_time;
		unsigned int participant_type;
		string conf_mg_uid;
        unsigned int rec_count;
		
	   sip_pbx_get_conference_register_info();
	   sip_pbx_get_conference_register_info(sip_pbx_get_conference_register_info &);
	   ~sip_pbx_get_conference_register_info();
	   void operator=(sip_pbx_get_conference_register_info &);
	   
};
