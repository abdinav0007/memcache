#include<iostream>
#include<string>
using namespace std;
class SIP_PBX_check_conference_id
{
	public:
	long errcode;
    string errmsg;
	string hostpin;
	string participant_pin;
	long screening_enabled;
	long recording_enabled;
	long conf_expired;
	SIP_PBX_check_conference_id();
	~SIP_PBX_check_conference_id();
	SIP_PBX_check_conference_id(const SIP_PBX_check_conference_id &);
	void operator =(const SIP_PBX_check_conference_id &);
	
};
