#include"UR_PBX_get_company_extension_hours_setting.h"



UR_PBX_get_company_extension_hours_setting::UR_PBX_get_company_extension_hours_setting()
{
      extension_flag=0;
	working_business_type="";
	company_hour=0;
	hour_format=0;
	comp_hours_ivr_type=0;
	comp_hours_ivr_file="";
	 comp_after_hours_ivr_type=0;
	comp_after_hours_ivr_file="";
	
}

UR_PBX_get_company_extension_hours_setting::~UR_PBX_get_company_extension_hours_setting()
{
	
}


UR_PBX_get_company_extension_hours_setting::UR_PBX_get_company_extension_hours_setting(const UR_PBX_get_company_extension_hours_setting &obj2)
{
		extension_flag=obj2.extension_flag;
		working_business_type=obj2.working_business_type;
		company_hour=obj2.company_hour;
		hour_format=obj2.hour_format;
		comp_hours_ivr_type=obj2.comp_hours_ivr_type;
		comp_hours_ivr_file=obj2.comp_hours_ivr_file;
		comp_after_hours_ivr_type=obj2.comp_after_hours_ivr_type;
		comp_after_hours_ivr_file=obj2.comp_after_hours_ivr_file;
}


void UR_PBX_get_company_extension_hours_setting :: operator=(const UR_PBX_get_company_extension_hours_setting  &obj3)
{
		extension_flag=obj3.extension_flag;
		working_business_type=obj3.working_business_type;
		company_hour=obj3.company_hour;
		hour_format=obj3.hour_format;
		comp_hours_ivr_type=obj3.comp_hours_ivr_type;
		comp_hours_ivr_file=obj3.comp_hours_ivr_file;
		comp_after_hours_ivr_type=obj3.comp_after_hours_ivr_type;
		comp_after_hours_ivr_file=obj3.comp_after_hours_ivr_file;
} 
