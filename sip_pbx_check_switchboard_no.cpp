#include<iostream>
#include<string>
using namespace std;
#include "sip_pbx_check_switchboard_no.h"

sip_pbx_check_switchboard_no::sip_pbx_check_switchboard_no()//default cons
	{
		errcode	= 0;
		errmsg = "";
		auto_routing = 0;	
		switcboardid ="";	
		swb_domainname ="";	
		swb_domainid ="";	
		opr_extension ="";		
	}
	
	
	sip_pbx_check_switchboard_no::~sip_pbx_check_switchboard_no()//destructor
	{
		
	}
	
	
	sip_pbx_check_switchboard_no::sip_pbx_check_switchboard_no(sip_pbx_check_switchboard_no &obj2)//copy constructor
	{
		errcode	= obj2.errcode;
		errmsg = obj2.errmsg;
		auto_routing = obj2.auto_routing;	
		switcboardid = obj2.switcboardid;	
		swb_domainname = obj2.swb_domainname;	
		swb_domainid = obj2.swb_domainid;	
		opr_extension = obj2.opr_extension;	
	}
	
       void sip_pbx_check_switchboard_no:: operator =(sip_pbx_check_switchboard_no &obj3)//assignment operator
       {
                errcode	= obj3.errcode;
		errmsg = obj3.errmsg;
		auto_routing = obj3.auto_routing;	
		switcboardid = obj3.switcboardid;	
		swb_domainname = obj3.swb_domainname;	
		swb_domainid = obj3.swb_domainid;	
		opr_extension = obj3.opr_extension;
	}
                
        

	
